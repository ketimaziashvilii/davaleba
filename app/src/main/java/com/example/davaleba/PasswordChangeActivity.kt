package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {

    private lateinit var editTextPassword: EditText
    private lateinit var editTextNewPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private lateinit var buttonChangePassword: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)


        init()

        registerListeners()


    }

    private fun init() {

        editTextNewPassword = findViewById(R.id.editTextPassword)
        editTextNewPassword = findViewById(R.id.editTextNewPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        buttonChangePassword = findViewById(R.id.buttonChangePassword)


    }

    private fun registerListeners() {
        buttonChangePassword.setOnClickListener {
            val newPassword = editTextNewPassword.text.toString()
            if (newPassword.isEmpty() || newPassword.length < 6)
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            return@setOnClickListener

        }

        FirebaseAuth.getInstance().currentUser?.updatePassword(editTextNewPassword.toString())
            ?.addOnCompleteListener { task ->

                if (task.isSuccessful) {
                    Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show()
                } else {

                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()

                }


            }
    }


}

